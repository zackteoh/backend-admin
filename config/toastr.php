<?php

return [

    'session_name' => 'toastr',

    'options' => [
        "closeButton" => true,
        "debug" => false,
        "newestOnTop" => false,
        "progressBar" => true,
        "positionClass" => "toast-bottom-right",
        "preventDuplicates" => true,
        "onclick" => null,
        "showDuration" => "10",
        "hideDuration" => "500",
        "timeOut" => "1500",
        "extendedTimeOut" => "1000",
        "showEasing" => "swing",
        "hideEasing" => "linear",
        "showMethod" => "fadeIn",
        "hideMethod" => "fadeOut"
    ]

];

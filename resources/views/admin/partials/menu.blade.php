<div class="media user-media dker">
        <div class="user-media-toggleHover">
            <span class="fa fa-user"></span>
        </div>
        <div class="user-wrapper">
            <a class="user-link" href="">
                <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture" src="{{ url(Auth::user()->profile_path) }}"><p class="text-white user-info">Welcome {{ ucwords(Auth::user()->name) }}</p></a>

            {{-- <div class="search_bar col-lg-12">
                <div class="input-group">
                    <input type="search" class="form-control " placeholder="search">
                    <span class="input-group-btn">
                        <button class="btn without_border" type="button"><span class="fa fa-search" >
                        </span></button>
                    </span>
                </div>
            </div> --}}
        </div>
    </div>
    <!-- #menu -->
    <ul id="menu" class="bg-blue dker">
        <li class="menu-item" link="{{ route('admin.dashboard.index') }}">
            <a href="{{ route('admin.dashboard.index') }}" class="link">
                <i class="fa fa-home"></i>
                <span class="link-title">&nbsp;Dashboard</span>
            </a>
        </li>
        <li class="parent">
            <a href="javascript:;">
                <i class="fa fa-users"></i>
                <span class="link-title">&nbsp; Users</span>
                <span class="fa arrow"></span>
            </a>
            <ul>
                <li class="menu-item" link="{{ route('admin.users.create') }}">
                    <a href="{{ route('admin.users.create') }}">
                        <i class="fa fa-angle-right"></i>
                        &nbsp; Create User
                    </a>
                </li>
                <li class="menu-item" link="{{ route('admin.users.index') }}">
                    <a href="{{ route('admin.users.index') }}">
                        <i class="fa fa-angle-right"></i>
                        &nbsp; Users List
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- /#menu -->
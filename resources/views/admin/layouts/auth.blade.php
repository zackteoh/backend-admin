<!doctype html>
<html class="no-js" lang="{{ Config::get('app.locale') }}">

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/logo1.ico') }}"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- global styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}"/>
    <!-- end of global styles-->
    @yield('css')
</head>

<body>
        <!--preloader-->
        {{-- @include('admin.partials.preloader') --}}

        <div class="container wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="2s">
            <div class="row">
                <div class="col-lg-8 push-lg-2 col-md-10 push-md-1 col-sm-10 push-sm-1 login_top_bottom">
                    <div class="row">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

    <!-- global scripts-->
    <script type="text/javascript" src="{{ asset('js/components.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrapValidator.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/login.js') }}"></script>
    <!-- end of global scripts-->
    @yield('js')
</body>
</html>
<!doctype html>
<html class="no-js" lang="{{ Config::get('app.locale') }}">

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/logo1.ico') }}"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- global styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}"/>
    @yield('css')
</head>

<body>
        <!--preloader-->
        @include('admin.partials.preloader')

        <div class="bg-dark" id="wrap">
            <div id="top">
               
            <!-- navbar --> 
            @include('admin.partials.navbar')

            </div>
            <!-- /#top -->

            <div id="left">
                <!-- navbar --> 
                @include('admin.partials.menu')
            </div>
            <!-- /#left -->
            <div id="content" class="bg-container">
                @yield('header')
                @if ($errors->any())
                <div class="alert alert-danger">{!! implode('<br/>', $errors->all(':message')) !!}</div>
                @endif
                <div class="outer">
                <div class="inner bg-light lter bg-container">
                    @yield('content')
                </div>
                <!-- /.inner -->
            </div>
            <!-- /.outer -->
            </div>
        <!-- /#content -->
             <!-- # right side -->
             @include('admin.partials.sidebar')
    </div>
    <!-- /#wrap -->

    <!-- global scripts-->
    <script type="text/javascript" src="{{ asset('js/components.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    
    {!! toastr()->render() !!}
    @if(session('toastr'))
        @php \Session::forget('toastr'); @endphp
    @endif
    <script>
    $(".logout").click(function(){
        $("#logout-form").submit();
    });
    $(".menu-item").each(function() {   
        if ($(this).attr('link') == window.location.href) {
            $(this).addClass("active");
            var parent = $(this).closest('.parent');
            if (parent.length) {
                parent.addClass("active");
            }
        }
    });
    </script>
    <!-- end of global scripts-->
    @yield('js')
</body>
</html>
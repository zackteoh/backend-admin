@extends('admin.layouts.master')

@section('title')
    Dashboard | {{ config('app.name') }}
@endsection

@section('css')
    
@endsection

@section('header')
<header class="head">
    <div class="main-bar">
        <div class="row">
            <div class="col-lg-6">
                <h4 class="nav_top_align">
                    <i class="fa fa-file-o"></i>
                    Dashboard
                </h4>
            </div>
            <div class="col-lg-6">
                <div class="float-right">
                    <ol class="breadcrumb nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="#">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                Dashboard
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
    <div class="jumbotron">
            <h2>Genius is one percent inspiration and ninety-nine percent perspiration.</h2> - Thomas Edison
    </div>
@endsection
<script>

</script>
@section('js')
    
@endsection
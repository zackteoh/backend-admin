@extends('admin.layouts.master')

@section('title')
    Create New User | {{ config('app.name') }}
@endsection

@section('css')
    
@endsection

@section('header')
<header class="head">
    <div class="main-bar">
        <div class="row">
            <div class="col-lg-6">
                <h4 class="nav_top_align">
                    <i class="fa fa-users"></i>
                    User List
                </h4>
            </div>
            <div class="col-lg-6">
                <div class="float-right">
                    <ol class="breadcrumb nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard.index') }}">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
							<a href="{{ route('admin.users.index') }}">Users</a>
						</li>
						<li class="breadcrumb-item active">
								Create Users
						</li>
                    </ol>
                    </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
<div class="card">
    <div class="card-header bg-white">
        Create New User
    </div>
    <div class="card-block">
        <form action="{{ route('admin.users.store') }}" method="post" enctype="multipart/form-data">
            @method('POST')
            @csrf
            @include('admin.users.form')
        </form>
    </div>
</div>
@stop

@section('js')
	<script>
		$(document).ready(function(){
            var date_input=$('input[name="dob"]'); //our date input has the name "date"
            var options={
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
		})
	</script>
@endsection

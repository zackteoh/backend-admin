@extends('admin.layouts.master')

@section('title')
    User List | {{ config('app.name') }}
@endsection

@section('css')
    <style>
    
    </style>
@endsection

@section('header')
<header class="head">
    <div class="main-bar">
        <div class="row">
            <div class="col-lg-6">
                <h4 class="nav_top_align">
                    <i class="fa fa-users"></i>
                    User List
                </h4>
            </div>
            <div class="col-lg-6">
                <div class="float-right">
                    <ol class="breadcrumb nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.dashboard.index') }}">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            Users
                        </li>
                    </ol>
                    </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
<div class="outer">
        <div class="inner bg-light lter bg-container">
            <div class="row">
                <div class="col-12 data_tables">
                    <div class="card">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> Users Datatable 
                        </div>
                        <div class="card-block p-t-25">
                            <div class="">
                                <div class="pull-sm-right">
                                    <div class="tools pull-sm-right"></div>
                                </div>
                            </div>
                            <table id="table_users" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>User Role</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>            
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    
@endsection

@section('js')
<script>
    $(document).ready(function(){
        var table_obj = $('#table_users').DataTable({
            processing: true,
            serverSide: true,
            searching: true,
            bLengthChange: true,
            ajax: {
                url: '{{ route('admin.users.getdatatables') }}',
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'roles', name: 'roles', orderable: false, searchable: false},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action'}
            ]
        });
        $('#table_users').on('click','.btn-danger', function(e){
            e.preventDefault();
            var id = $(this).data('id');
            if (confirm('Delete this user?')) {
                $("#delete_"+id).submit();
            } else {
                alert('It has been cancelled!');
            }
            
        })
    });
</script>
@endsection
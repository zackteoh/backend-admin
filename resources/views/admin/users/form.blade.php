<div class="row mB-40">
	<div class="col-sm-12">
		<div class="bgc-white p-20 bd">
			<div class="col-md-12 col-sm-12">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<label for="name">Full Name</label>
							<input type="text" class="form-control" name="name" placeholder="Full Name" value="{{ isset($user)?$user->name: old('name') }}">
						</div>
						@if(!isset($user))
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" name="password" placeholder="Password" }}">
						</div>
						<div class="form-group">
							<label for="confirmed">Confirm Password</label>
							<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" }}">
						</div>
						@endif
						<div class="form-group">
							<label for="dob">Date Of Birth</label>
							<input type="text" class="form-control" name="dob" placeholder="DOB"  value="{{ isset($user)?$user->dob:old('dob') }}">
						</div>
						<div class="form-group">
							<label for="user_role">Role</label>
							<select name="user_role_id" id="user_role_id" class="form-control">
								<option value="-1">Select Role</option>
								@php
									{{ $selectedvalue = (isset($user)) ? $user->user_role_id : old('user_role_id'); }}
								@endphp
								@foreach($roles as $role)
									<option value="{{$role['id']}}"  {{ $selectedvalue == $role['id'] ? 'selected="selected"' : '' }}>{{$role['name']}}</option>
								@endforeach 
							</select>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<label for="phone">Phone</label>
							<input type="number" class="form-control" name="phone" placeholder="Phone"  value="{{ isset($user)?$user->phone:old('phone') }}">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ isset($user)?$user->email:old('email') }}">
						</div>
						<div class="form-group">
							<label for="gender">Gender</label>
							<select name="gender" id="gender" class="form-control">
								<option value="-1">Select Gender</option>
								@php
									{{ $selectedvalue = (isset($user)) ? $user->gender : old('gender'); }}
								@endphp
								@foreach($genders as $key=>$gender)
									<option value="{{$key}}"  {{ $selectedvalue == $key ? 'selected="selected"' : old('gender') }}>{{  $gender }}</option>
								@endforeach 
							</select>
						</div>
						<div class="form-group">
							<label for="status">Status</label>
							<select name="status" id="status" class="form-control">
								<option value="-1">Select Status</option>
								@php
									{{ $selectedvalue = (isset($user)) ? $user->status :-1; }}
									if(old('status')!==null)
									{
										$selectedvalue = old('status');
									}
								@endphp
								@foreach($statuses as $key=>$status)
									<option value="{{$key}}"  {{ $selectedvalue == $key ? 'selected="selected"' : '' }}>{{  $status }}</option>
								@endforeach 
							</select>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="row">
							<div class="col-md-12 col-sm-12">
									<label for="upload_profile">Upload Profile Image</label>
									<input type="file" name="upload_profile" class="form-control" />
							</div>
							<div class="col-md-12 col-sm-12">
								@if(isset($user))
									<img src="{{ url($user->profile_path) }}" width="100px" height="auto" alt="image01.png"/>
								@endif
							</div>
						</div>						
					</div>
				</div>
				
				
				<div>
					<button type="submit" class="btn btn-primary">{{ !isset($user)? "Create" : "Update" }}</button>
					<a href="{{ url()->previous() }}" class="btn btn-danger"> Back </a>
				</div>
			</div>
		</div>  
	</div>
</div>

@extends('admin.layouts.auth')

@section('content')
<div class="col-lg-8 push-lg-2 col-md-10 push-md-1 col-sm-12">
        <div class="login_logo login_border_radius1">
            <h3 class="text-center">
                <img src="{{ asset('img/logow.png') }}" alt="josh logo" class="admire_logo"><span class="text-white"> {{ config('app.name') }} &nbsp;<br/>
                    Reset Password</span>
            </h3>
        </div>
        <div class="bg-white login_content login_border_radius">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form autocomplete="false" method="POST"  action="{{ route('password.email') }}">
                @csrf
                <div class="form-group">
                    <label for="email" class="col-form-label"> E-mail</label>
                    <div class="input-group">
                        <span class="input-group-addon input_email"><i
                                class="fa fa-envelope text-primary"></i></span>
                        <input autocomplete="false" type="text" class="form-control  form-control-md" id="email" name="email" placeholder="E-mail"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="submit" value="Send Reset Password Link" class="btn btn-primary btn-block login_button">
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group">
                <div class="row">
                    <div class="col-12 text-right forgot_pwd">
                        <a href="{{ route('login') }}" class="custom-control-description forgottxt_clr">Back To Login</a>
                    </div>
                </div>
            </div>
            {{-- <div class="form-group">
                <label class="col-form-label">Don't you have an Account? </label>
                <a href='register.html' class="text-primary login_hover"><b>Sign Up</b></a>
            </div> --}}
        </div>
    </div>
@endsection

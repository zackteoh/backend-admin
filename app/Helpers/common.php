<?php

if (! function_exists('uuid')) {
    function uuid() {
        $uuid = (string) Str::uuid();
        $arr = explode('-',$uuid);
        return $arr[0].$arr[1];
    }
}

if (! function_exists('uploadFile')) {
    function uploadFile($file,$upload_folder) {
        $base_folder = 'uploads';
        $rand_name = uuid();
        $extension = $file->getClientOriginalExtension();
        $uploaded = Storage::disk(meta('storage'))->put($upload_folder.'/'.$rand_name.'.'.$extension,  File::get($file));
        if(!$uploaded){
            die();
            return "Failed to upload at some point!";
        }
        $path = $base_folder.'/'.$upload_folder.'/'.$rand_name.'.'.$extension; 
        return $path;
    }
}

if (! function_exists('meta')) {
    function meta($key) {
        $meta = \DB::table('metadata')->where('key',$key)->first();
        return $meta->value;
    }
}

if (! function_exists('removeFile')) {
    function removeFile($path) {
        $deleted_path = public_path().'/'.$path;
        $existed = file_exists($deleted_path) ? true : false;
        if($existed){
            File::delete($path);
            return true;
        }else{
            return false;
        }
    }
}
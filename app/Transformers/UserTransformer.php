<?php 
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;
use Spatie\Permission\Models\Role;
use Auth;
class UserTransformer extends TransformerAbstract
{
    /**
     * @return  array
     */
    public function transform(User $user)
    {
        $user = User::find($user->id);
        $role = Role::find($user->user_role_id);
        $action = '<div style="display:inline-flex;"><a href="/admin/users/'.$user->id.'/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a><form id="delete_'.$user->id.'" action="/admin/users/'.$user->id.'" method="POST"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="'.csrf_token().'"><button class="btn btn-xs btn-danger" data-id="'.$user->id.'"><i class="fa fa-times"></i></button></form></div>';
        return [
            'id'         => (int) $user->id,
            'name'       => $user->name,
            'email'      => $user->email,
            'phone'      => $user->phone,
            'roles'      => $role->name,
            'status'     => ($user->status == 1) ? "Active" : "Deactivated",
            'created_at' => $user->created_at->toDateTimeString(),
            'action'     => $action,
        ];
    }
}
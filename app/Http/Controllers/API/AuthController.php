<?php

namespace App\Http\Controllers\API;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Hash;
use Auth;

class AuthController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required',
            'phone' => 'required|unique:users,phone',
            'user_role_id' => 'required',
            'c_password' => 'required|same:password',
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(),401);       
        }


        $input = $request->all();
        if($request->input('user_role_id')==1){
            return $this->sendError('Create admin is prohibited through this api!',402);
        }

        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('NIAG_CLIENT_REGISTER')->accessToken;
        $success['name'] =  $user->name;

        $userType = Role::find($input['user_role_id']);

        return $this->sendResponse($success, $userType->name.' register successfully.');
    }
    public function login (Request $request) {

        $user = User::where('email', $request->email)->first();
    
        if ($user) {
    
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('NIAG_CLIENT_LOGIN')->accessToken;
                return $this->sendResponse(['token'=>$token], 'Login successfully.');
            } else {
                $response = "Password missmatch";
                return $this->sendError("Failed to Login",$response,401);
            }
    
        } else {
            $response = 'User does not exist';
            return $this->sendError("Failed to Login", $response,404);
        }
    
    }
    public function logout (Request $request) {

        $user = Auth::user();
        $data = [
            'id'=>$user->id,
            'email'=>$user->email
        ];
        $token = $request->user()->token();
        $token->revoke();
    
        $response = 'You have been succesfully logged out!';
        return $this->sendResponse(['user'=>$data], $response);
    
    }

    public function loggedInUser(Request $request)
    {
        $user = Auth::guard()->user();
        return $this->sendResponse($user, 'User Details');
    }

}

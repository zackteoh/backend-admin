<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 1, 'user_role_id' => 1])) {
            // return redirect()->intended('dashboard');
        }  else {
            $this->incrementLoginAttempts($request);
            $user = \App\User::whereEmail($request->email)->first();
            if(empty($user)){
                return Redirect::back()->withInput($request->all())->withErrors(["Email not found!"]);
            }
            if($user->status != 1){
                $errors[] = "This account is not activated";
            }else if($user->user_role_id != 1){
                $errors[] = "Only admin allowed to access!";
            }else{
                $errors[] = "Incorrect credentials provided!";
            }
            return Redirect::back()->withInput($request->all())->withErrors($errors);
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('login');
    }
}

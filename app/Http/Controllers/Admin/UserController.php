<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\User;
use Illuminate\Support\Facades\Input;
use Validator, Redirect, DataTables, Auth, Toastr,Storage,File;
use App\Helpers\common;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //get roles, gender, status pre-defined data
        $data = $this->modelData();
        return view('admin.users.create',['roles'=>$data['roles'],'genders'=>$data['genders'],'statuses'=>$data['statuses']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //server side validation
        $validator = Validator::make($request->all(),User::rules());
        if ($validator->fails()) {
            return Redirect::back()->withInput(Input::all())->withErrors($validator);
        }
        //custom validation
        $errors = $this->customValidation($request->all());
        if(!empty($errors)){
            return Redirect::back()->withInput(Input::all())->withErrors($errors);
        }
        //end validation
        
        //create user
        $user = User::create($request->all());

        //if has upload image
        $file = $request->file('upload_profile');
        if($file){   
            $profile_path = uploadFile($file,'users');
            $user->profile_path = $profile_path;
            $user->save();
        }

        //Display Message and Redirect
        $role = Role::find($request->input('user_role_id'));
        Toastr::info("Create $role->name Successfully!");
        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::find($id);
        $user->dob = \Carbon\Carbon::parse($user->dob)->format('Y-m-d');
        $data = $this->modelData();
        return view('admin.users.edit',['user'=>$user,'roles'=>$data['roles'],'genders'=>$data['genders'],'statuses'=>$data['statuses']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //server side validation
        $validator = Validator::make($request->all(),User::rules(true,$id));
        if ($validator->fails()) {
            return Redirect::back()->withInput(Input::all())->withErrors($validator);
        }
        //custom validation
        $errors = $this->customValidation($request->all());
        if(!empty($errors)){
            return Redirect::back()->withInput(Input::all())->withErrors($errors);
        }
        //end validation

        $user = User::findOrFail($id);
        $user->update($request->all());

        //if has upload image
        $file = $request->file('upload_profile');
        if($file){   
            //remove existing file
            removeFile($user->profile_path);
            //upload image and set updated path
            $profile_path = uploadFile($file,'users');
            $user->profile_path = $profile_path;
            $user->save();
        }
        //display message and done
        Toastr::info('Update User Successfully!');
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('users')->where('id',$id)->delete();
        Toastr::info('User is removed successfully!');
        return redirect()->route('admin.users.index');
    }

    public function getdatatables()
    {
        $user = Auth::user();
        $users = \App\User::select(['id', 'name', 'email', 'phone', 'user_role_id', 'status', 'created_at']);
        return DataTables::of($users)
            ->addColumn('action', function ($user) {
            })
            ->setTransformer(new \App\Transformers\UserTransformer)
            ->make(true);
    }

    public function customValidation($request)
    {
        $errors = array();
        if($request['gender'] == -1){
            $errors[] = "The field gender is required!";
        }
        if($request['status'] == -1){
            $errors[] = "The field status is required!";
        }
        if($request['user_role_id'] == -1){
            $errors[] = "The field role is required!";
        }
        return $errors;
    }
    public function modelData()
    {
        $roles = Role::get();
        $genders = \App\User::getGender();
        $statuses = \App\User::getStatus();
        return ['roles'=>$roles,'genders'=>$genders,'statuses'=>$statuses];
    }
}

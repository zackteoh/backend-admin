<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'gender', 'dob', 'user_role_id', 'status', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id = null)
    {
        $commun = [
            'name'              => "required",
            'phone'             => "required",
            'email'             => "required|email|max:255|unique:users,email,$id",
            'password'          => 'nullable|min:6|confirmed',
            'gender'            => 'required',
            'dob'               => 'required|date',
            'status'            => 'required',
            'user_role_id'      => 'required',
            'upload_profile'    => 'nullable|file|image|mimes:jpeg,png,gif,webp|max:2048',
        ];

        if ($update) {
            return $commun;
        }

        return array_merge($commun, [
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }
    public static function getGender($id=null)
    {
        $v[1] = "Male";
        $v[2] = "Female";
        if($id){
            return $v[$id];
        }
        return $v;
    }

    public static function getStatus($id=null)
    {
        $v[1] = "Active";
        $v[0] = "Inactive";
        if($id){
            return $v[$id];
        }
        return $v;
    }
     /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
    public function setPasswordAttribute($value='')
    {
        $this->attributes['password'] = bcrypt($value);
    }
    
    public function getProfilePathAttribute($value)
    {
        if (!$value) {
            return 'http://placehold.it/160x160';
        }
    
        return $value;
    }
    public function getRoleName()
    {
        $role = Role::find($this->user_role_id);
        return $role->name;
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@nightingale.com',
                'phone' => '60165487339',
                'gender' => '1',
                'dob' => '1987-01-01',
                'user_role_id' => '1',
                'status' => '1',
                'password' => Hash::make('secret'),
                'created_at'  => \Carbon\Carbon::now(),
                'updated_at'  => \Carbon\Carbon::now()
            ],
            [
                'name' => 'adele',
                'email' => 'adele@nightingale.com',
                'phone' => '60165587339',
                'gender' => '2',
                'dob' => '1990-01-01',
                'user_role_id' => '2',
                'status' => '1',
                'password' => Hash::make('secret'),
                'created_at'  => \Carbon\Carbon::now(),
                'updated_at'  => \Carbon\Carbon::now()
            ],
            [
                'name' => 'sam smith',
                'email' => 'sam@nightingale.com',
                'phone' => '60165597339',
                'gender' => '1',
                'dob' => '1988-01-01',
                'user_role_id' => '3',
                'status' => '1',
                'password' => Hash::make('secret'),
                'created_at'  => \Carbon\Carbon::now(),
                'updated_at'  => \Carbon\Carbon::now()
            ],
        ];

        DB::table('users')->insert($users);
    }
}

<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        $now = \Carbon\Carbon::now();

        $permissions = [
            // User
            ['name' => 'list_admin'],
            ['name' => 'create_admin'],
            ['name' => 'update_admin'],
            ['name' => 'destroy_admin'],
        ];
        foreach ($permissions as $key => $permission) {
            $permissions[$key]['guard_name'] = 'web';
            $permissions[$key]['created_at'] = $now;
            $permissions[$key]['updated_at'] = $now;
        }

        Permission::insert($permissions);
        //sync permission with role
        $admin = Role::find(1);
        $admin->syncPermissions([1,2,3,4]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MetadataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'  =>'Application Name',
                'key'   =>'app_name',
                'value' =>'Nightingale',
            ],
            [
                'name'  =>'Storage',
                'key'   =>'storage',
                'value' =>'public',
            ],
            [
                'name'  =>'Application Version',
                'key'   =>'app_version',
                'value' =>'1.0',
            ],
        ];
        DB::table('metadata')->insert($data);
    }
}

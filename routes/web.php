<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::group(['prefix' => 'admin','middleware' => 'auth'], function () {
    //Dashboard
    Route::get('/dashboard','Admin\DashboardController@index')->name('admin.dashboard.index');
    //User
    Route::get('/users/getdatatables','Admin\UserController@getdatatables')->name('admin.users.getdatatables');
    Route::resource('users','Admin\UserController',['as'=>'admin']);
});

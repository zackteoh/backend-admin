<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['json.response']], function () {

    // public routes
    Route::post('/login', 'API\AuthController@login')->name('login.api');
    Route::post('/register', 'API\AuthController@register')->name('register.api');

    // private routes
    Route::middleware('auth:api')->group(function () {
        //Auth
        Route::post('/user','API\AuthController@loggedInUser');
        Route::get('/logout', 'API\AuthController@logout')->name('logout');
        //
    });

});